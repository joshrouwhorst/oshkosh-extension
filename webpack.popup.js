const mode = 'production'
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const defaultOpts = {
    optimization: {
        minimize: false
    },
    mode
}

const config = {
    entry: './src/popup/index.js',
    output: {
        path: __dirname + '/public/popup',
        filename: 'index.js',
        publicPath: '',
    },
    devServer: {
        static: {
            directory: __dirname + '/public/popup'
        },
        port: 9000,
        client: {
            overlay: {
                warnings: false
            }
        }
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/popup/index.html'
        })
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    "style-loader",
                    // Translates CSS into CommonJS
                    "css-loader",
                    // Compiles Sass to CSS
                    "sass-loader",
                ],
            },
        ]
    },
    ...defaultOpts
}

module.exports = config