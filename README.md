# OshKosh Extension

- [OshKosh Extension](#oshkosh-extension)
  - [Quick changes](#quick-changes)
  - [Links](#links)
  - [Deployment Steps](#deployment-steps)
  - [Setting Environment Variables](#setting-environment-variables)
  - [Error Decoding Signature](#error-decoding-signature)

## Quick changes

1. Make your update to the scripts in the `src` folder.
2. Once you're done run `npm run push`. This will build and push your code to git.
3. Use the `oshupdate` URL parameter to force an update.

## Links

- [Mozilla Developer Hub](https://addons.mozilla.org/en-US/developers/)
  - Manage API keys and info on extensions
- [Firefox Extension Development Docs](https://extensionworkshop.com/documentation/develop/)

## Deployment Steps

1. Make changes to code in `extension` folder.
2. Run `npm run start` to debug in a browser window.
3. When you're good with changes, update the version in `extension/manifest.json`.
4. Run `npm run build`, which should generate a zip file in `extension/web-ext-artifacts`.
5. Run `npm run sign`, which should generate an xpi file in `extension/web-ext-artifacts`.
6. Update the `extension/updates.json` file to have an entry for this version, commit, and push the updates.
7. You should be able to go into Manage Addons in Firefox and check for updates and it'll update the extension.

## Setting Environment Variables

Your Mozilla API key and secret should be set to `WEB_EXT_API_KEY` and `WEB_EXT_API_SECRET` environment variables. Had a pain trying to figure out setting environment variables in the latest macOS. But you put them in the `~/.zshenv` file. There are other settings you can put in there for `web-ext`. But those are the only ones that are pretty vital for signing. The `web-ext sign` process will look for those variables.

## Error Decoding Signature

Going to the Developer Hub and getting new credentials worked to solve this issue.
