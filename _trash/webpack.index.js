/*
TODO:
- Setup a config for building index.js
- Set this up as a config just for oshkosh.js so we can pull in other libraries.
- Set up index.js to immediately run the oshkosh it has stored if available, and load the script if it's been 24 hours since the last time it loaded.
- Set up a way to manually trigger index.js to load the script. Could be a URL parameter or a menu icon option.
- Look into having the oshkosh script load actions in the menu.
*/

module.exports = {
    entry: './src/index.js',
    mode: 'development',
    output: {
        path: __dirname + '/extension',
        filename: 'index.js'
    }
}