(() => {
    // eslint-disable-next-line
    if (window.location.href.indexOf('oshdebug') > -1) debugger
})()

const WATCH_URL_INTERVAL = 500;

class Registry {
    constructor() {
        this.Scripts = []
        this.ActiveScripts = []
    }

    RunScripts() {
        if (window.location.href.indexOf('stop.scripts') > -1) return

        const stopScript = GetQueryVariable('stop.script')
        if (!!stopScript) {
            let idx = this.ActiveScripts.findIndex((s) => s.Name === stopScript)
            if (idx > -1) this.ActiveScripts.splice(idx, 1)
        }

        if (this.ActiveScripts.length === 0) return

        const names = this.ActiveScripts.map(s => s.Name)
        Log(`Running ${names.join(', ')}`)
        this.ActiveScripts.forEach(s => s.Run())
    }

    RegisterScript(script) {
        this.Scripts.push(script)

        let match = false
        if (!script.Rules) match = true
        else {
            if (!match && script.Rules.includes) match = script.Rules.includes.some(i => window.location.href.indexOf(i) > -1)
            if (match && script.Rules.excludes) match = !script.Rules.excludes.some(i => window.location.href.indexOf(i) > -1)
        }

        if (match) this.ActiveScripts.push(script)
    }
}

const registry = new Registry()

class Script {
    constructor({ name, rules }) {
        this.Name = null
        this.Rules = null

        if (name) this.Name = name
        if (rules) this.Rules = rules
    }
}

/*
*
* Sitefinity
*
*/

class SitefinityBar extends Script {
    constructor() {
        super({
            name: 'Sitefinity Bar',
            rules: {
                includes: [
                    '/sitefinity',
                    '/Sitefinity'
                ]
            }
        })

        this.Links = [
            {
                name: 'Pages',
                link: '/Sitefinity/Pages',
            },
            {
                name: 'Images',
                link: '/Sitefinity/Content/Images'
            },
            {
                name: 'Documents',
                link: '/Sitefinity/Content/Documents'
            },
            {
                name: 'Labels',
                link: '/Sitefinity/Administration/Labels'
            },
            {
                name: 'Advanced Settings',
                link: '/Sitefinity/Administration/Settings/Advanced'
            },
            {
                name: 'Translations Settings',
                link: '/Sitefinity/Administration/Settings/Advanced/Translations'
            }
        ]
    }

    Run() {
        if ($('#oshkosh-sf-bar').length > 0) return

        let links = ''

        for (let i = 0; i < this.Links.length; i++) {
            let link = this.Links[i]
            links += `<a style="display: inline-block; color: white; padding: 5px 0 5px 20px;" href="${link.link}">${link.name}</a>`
        }
        $('body').prepend(`
        <div id="oshkosh-sf-bar" style="background-color: black;">
            ${links}
        </div>
        `)
    }
}

/*
*
* APTIV
*
*/

class AptivLinkFixer extends Script {
    constructor() {
        super({
            name: 'Aptiv Link Fixer',
            rules: {
                includes: [
                    'aptiv.springthroughtest.com',
                    'aptivtest.azurewebsites.net',
                    'origin.aptivstaging.com',
                    'aptivdev.azurewebsites.net'
                ]
            }
        })

        this.Host = window.location.host
    }

    Run() {
        $(() => {
            const links = $('a[href]')
            for (var i = 0; i < links.length; i++) {
                while (links[i].href.indexOf('www.aptiv.com') > -1) {
                    links[i].href = links[i].href.replace('www.aptiv.com', this.Host)
                }
            }
        })
    }
}

class AptivCookieBanner extends Script {
    constructor() {
        super({
            name: 'Aptiv Cookie Banner'
        })

        this.MaxTries = 40
        this.Tries = 0
        this.LoopTime = 500
    }

    Run() {
        this.TryClose()
    }

    TryClose() {
        const banner = $('#onetrust-banner-sdk')
        if (banner.length > 0) {
            banner.remove()
        } else if (this.Tries++ < this.MaxTries) {
            setTimeout(() => this.TryClose(), this.LoopTime)
        }
    }
}

/*
*
* AMAZON
*
*/

class AmazonSmile extends Script {
    constructor() {
        super({
            name: 'Amazon Smile',
            rules: {
                includes: [
                    'amazon.com'
                ]
            }
        })
    }

    Run() {
        try {
            if (window.location.href.indexOf('smile.amazon.com') <= -1) {
                const here = new URL(window.location.href)
                const next = new URL(here.href.replace(here.hostname, 'smile.amazon.com'))
                window.location.href = next.href
            }
        } catch (err) {
            Err('Amazon Smile - Run Err')
            Err(err)
        }
    }
}

/*
*
* Bing
*
*/

class BingFixer extends Script {
    constructor() {
        super({
            name: 'Bing Fixer',
            rules: {
                includes: [
                    'bing.com'
                ]
            }
        })
    }

    Run() {
        this.ClosePopup()
        this.RemoveSaveOption()
    }

    ClosePopup() {
        const selector = '#signout_closebtn'
        setTimeout(() => this.ClosePopup(), 1000)
    }

    RemoveSaveOption() {
        $('[aria-label="Save Control"]').remove()
        setTimeout(() => this.RemoveSaveOption(), 1000)
    }
}

/*
*
* FACEBOOK
*
*/

class FacebookFixer extends Script {
    constructor() {
        super({
            name: 'Facebook Fixer',
            rules: {
                includes: [
                    'facebook.com'
                ]
            }
        })

        this.LoopTime = 500
        this.AdsRemoved = 0
        this.Settings = {}
        this.LocalStorageName = 'FacebookFixerSettings'

        this.GetSettings()
    }

    Run() {
        this.SetupMain()
        this.Loop()
    }

    SetupMain() {
        const elem = $('div[role="main"]')
        if (elem.length <= 0) return Log('Did not find a Main element')

        if (this.Settings.HideMain) {
            const button = $('<button style="margin: 25px auto; padding: 25px; font-size: 1.3em;">Show Main</button>')
            button.on('click', () => {
                this.Settings.HideMain = false
                this.SaveSettings()
                window.location.reload()
            })
            elem.remove()
            button.prependTo('div[data-pagelet="LeftRail"]')
        } else {
            const button = $('<button style="margin: 25px auto; padding: 25px; font-size: 1.3em;">Hide Main</button>')
            button.on('click', () => {
                button.remove()
                this.Settings.HideMain = true
                this.SaveSettings()
                this.SetupMain()
            })
            button.prependTo('div[data-pagelet="LeftRail"]')
        }
    }

    Loop() {
        this.CheckFeed()
        this.AdLinks()
        this.RightRail()
        setTimeout(() => this.Loop(), this.LoopTime)
    }

    CheckFeed() {
        try {
            const elems = $('a[aria-label="Sponsored"],div[aria-label="Sponsored"]').closest('div[role="article"]')
            elems.replaceWith('<div>Ad Removed</div>')
            if (elems.length > 0) this.AdsRemoved += elems.length
        } catch (err) {
            Err('Facebook Fixer - Check Feed Err')
            Err(err)
        }
    }

    AdLinks() {
        try {
            const elems = $('a[aria-label="Advertiser link"]')
            elems.replaceWith('<div>Ad Removed</div>')
            if (elems.length > 0) this.AdsRemoved += elems.length
        } catch (err) {
            Err('Facebook Fixer - Check Feed Err')
            Err(err)
        }
    }

    RightRail() {
        const elem = $('div[data-pagelet="RightRail"]:not([data-fixed="true"])')
        if (elem.length <= 0) return
        elem.closest('div[role="complementary"]').remove()
        elem.attr('data-fixed', 'true')
    }

    GetSettings() {
        const settings = localStorage.getItem(this.LocalStorageName)
        if (settings) this.Settings = JSON.parse(settings)
    }

    SaveSettings() {
        const settings = JSON.stringify(this.Settings)
        localStorage.setItem(this.LocalStorageName, settings)
    }
}

/*
*
* SITEINFO
*
*/

class SiteInfo extends Script {
    constructor() {
        super({
            name: 'Site Info'
        })
    }

    Run() {
        const metas = []
        const openGraph = []
        const twitter = []
        let charset = null

        const metaTags = document.getElementsByTagName('meta')

        for (let i = 0; i < metaTags.length; i++) {
            const tag = metaTags[i]
            const { name, content, property } = tag
            const char = tag.charset
            const isTwitter = name ? name.includes('twitter:') : false

            if (isTwitter && name && content) twitter.push({ name, content })
            else if (name && content) metas.push({ name, content })
            else if (char) charset = char
            else if (property && content) openGraph.push({ property, content })
        }

        console.log('************** SITE INFO ****************')

        if (charset) log(`Charset: ${charset}`)

        if (metas.length > 0) header('Meta')

        for (let i = 0; i < metas.length; i++) {
            const { name, content } = metas[i]
            log(`${name}:    ${content}`)
        }

        if (openGraph.length > 0) header('OpenGraph')

        for (let i = 0; i < openGraph.length; i++) {
            const { property, content } = openGraph[i]
            log(`${property}:    ${content}`)
        }

        if (twitter.length > 0) header('Twitter')

        for (let i = 0; i < twitter.length; i++) {
            const { name, content } = twitter[i]
            log(`${name}:    ${content}`)
        }

        log('')
        console.log('*****************************************')

        function log(text) {
            console.log(`** ${text}`)
        }

        function header(text) {
            log('')
            log(`<><><> ${text} <><><>`)
        }
    }
}

/*
*
* POWERMAPPER
*
*/

class Powermapper extends Script {
    constructor() {
        super({
            name: 'Powermapper Reports',
            rules: {
                includes: ['powermapper.com']
            }
        })
    }

    Run() {
        if (window.location.pathname === '/') {
            this.HandleHomepage()
        } else if (window.location.pathname.toLowerCase().indexOf('/scans/history') > -1) {
            this.HandleScanHistory()
        }
    }

    HandleHomepage() {
        $('tbody > tr > td > a').each((idx, item) => {
            const $item = $(item)
            const customer = $item.text()

            const url = this.CheckLink($item.attr('href'))
            fetch(url).then(resp => resp.text()).then((data) => {
                const $page = $(data)
                let gotFirst = false
                Log(`Got scan page for ${customer}`)

                // Getting the report
                try {
                    $page.find('tbody > tr > td > a').each((idx, item) => {
                        if (gotFirst) return
                        gotFirst = true
                        const $scanItem = $(item)
                        const url = this.CheckLink($scanItem.attr('href'))
                        fetch(url).then(resp => resp.text()).then((data) => {
                            const $nextPage = $(data)
                            const link = $nextPage.find('#SelectExportFormat>option:contains("Issue Report (Excel CSV)")').attr('value')
                            $item.after('<span> - <a href="' + link + '">Report</a></span>')
                        }).catch((err) => {
                            Err('Error trying to get report page.')
                            Err(err)
                        })
                    })
                } catch (err) {
                    Err(`Error trying to get report for ${customer}`)
                    Err(err)
                }

                try {
                    Log(`Looking for delete page for ${customer}`)
                    const linkarr = $page.find('a').toArray()
                    const deleteLink = linkarr.find(link => link.href && link.href.indexOf && link.href.indexOf('/Scans/Delete') > -1)
                    if (deleteLink) {
                        const url = this.CheckLink(deleteLink.href)
                        fetch(url).then(resp => resp.text()).then((data) => {
                            const $delPage = $(data)
                            const $form = $delPage.find('form')
                            if (!$form || $form.length === 0) return
                            const formAction = $form.attr('action')
                            const scanIds = $delPage.find('input[name="ProcessIds"]').toArray().map(c => c.value)
                            const token = $delPage.find('input[name="__RequestVerificationToken"]').attr('value')

                            if (scanIds.length <= 1) return
                            const deleteIds = scanIds.slice(1, scanIds.length)
                            const $link = $('<button>Delete Old Scans</button>')
                            $link.on('click', () => this.DeleteOldScans(deleteIds, formAction, token, $link))
                            $item.after($link)
                        }).catch((err) => {
                            Err('Error collecting delete page.')
                            Err(err)
                        })
                    }
                } catch (err) {
                    Err(`Error trying to get delete scan ids for ${customer}`)
                    Err(err)
                }
            }).catch((err) => {
                Err(`Error trying to go to scan page.`)
                Err(err)
            })
        })
    }

    HandleScanHistory() {
        $('tbody > tr > td > a').each((idx, item) => {
            const $item = $(item)
            const url = this.CheckLink($item.attr('href'))
            fetch(url).then(resp => resp.text()).then((data) => {
                const $page = $(data)
                const link = $page.find('#SelectExportFormat>option:contains("Issue Report (Excel CSV)")').attr('value')
                $item.after('<span> - <a href="' + link + '">Report</a></span>')
            }).catch((err) => {
                Err('Error getting scan history')
                Err(err)
            })
        })
    }

    DeleteOldScans(ids, formAction, token, $link) {
        if (!confirm(`Are you sure you want to delete ${ids.length} old scan(s)?`)) return

        const url = this.CheckLink(formAction)
        fetch(url, { method: 'POST', data: { ProcessIds: ids, __RequestVerificationToken: token } }).then((data) => {
            $link.text('Success')
        }).catch((err) => {
            $link.text('Error')
            Err('Error trying to delete scans.')
            Err(err)
        })
    }

    CheckLink(href) {
        if (href.indexOf(window.location.origin) !== 0) {
            return window.location.origin + href
        } else {
            return href
        }
    }
}

/*
*
* YOUTUBE
*
*/

class YouTube extends Script {
    constructor() {
        super({
            name: 'YouTube',
            rules: {
                includes: ['youtube.com']
            }
        })

        this.LoopTime = 500
        this.MaxTries = 10
    }

    Run() {
        this.HideChat()
        this.HideComments()
        this.ShowDescription()
        this.CloseAd()
        this.AutoSkip()
    }

    HideChat(tries) {
        try {
            if (!tries) tries = 0

            const $elem = $('#show-hide-button a')
            if ($elem.length <= 0 && tries < this.MaxTries) return setTimeout(() => this.HideChat(tries), this.LoopTime)
            if ($elem.length > 0) {
                Log('YouTube Chat Collapsed')
                return $elem[0].click()
            }

            setTimeout(() => this.HideChat(tries + 1), this.LoopTime)
        } catch (err) {
            Err('Error running YouTube Chat Collapse')
            Err(err)
        }
    }

    HidePopup(tries) {
        try {
            if (!tries) tries = 0

            if ($('ytd-mealbar-promo-renderer #dismiss-button').length <= 0 && tries < this.MaxTries) {
                setTimeout(() => this.HidePopup(tries + 1), this.LoopTime)
            }

            $('ytd-mealbar-promo-renderer #dismiss-button').trigger('click')
            Log('YouTube popup exited.')
        } catch (err) {
            Err('Error trying to hide popup')
            Err(err)
        }
    }

    HideComments(tries) {
        try {
            if (!tries) tries = 0

            if (window.location.href.indexOf('show.comments') > -1) return

            if ($('#comments').length === 0 && tries < this.MaxTries) {
                return setTimeout(() => this.HideComments(tries + 1), this.LoopTime)
            }

            $('#comments').hide()
        } catch (err) {
            Err('Error running YouTube Hide Comments')
            Err(err)
        }
    }

    ShowDescription(tries) {
        try {
            if (!tries) tries = 0

            if ($('.more-button').length === 0 && tries < this.MaxTries) {
                return setTimeout(() => this.ShowDescription(tries + 1), this.LoopTime)
            }

            $('.more-button').trigger('click')

            // When switching routes, it might pick up on the previous more-button, so keep trying.
            if (tries < this.MaxTries) {
                return setTimeout(() => this.ShowDescription(tries + 1), this.LoopTime)
            }
        } catch (err) {
            Err('Error running YouTube Show Description')
            Err(err)
        }
    }

    CloseAd() {
        try {
            $('.ytp-ad-overlay-close-button').trigger('click')
            setTimeout(() => this.CloseAd(), this.LoopTime)
        } catch (err) {
            Err('Error running YouTube Close Ad')
            Err(err)
        }
    }

    AutoSkip() {
        try {
            $('.ytp-ad-skip-button-text').trigger('click')
            setTimeout(() => this.AutoSkip(), this.LoopTime)
        } catch (err) {
            Err('Error running YouTube AutoSkip')
            Err(err)
        }
    }
}

/*
*
* REDDIT
*
*/

class Reddit extends Script {
    constructor() {
        super({
            name: 'Reddit',
            rules: {
                includes: ['reddit.com']
            }
        })

        this.LoopTime = 500
        this.Tries = 0
        this.MaxTries = 10
    }

    Run() {
        this.Loop()
    }

    Loop() {
        this.KillAds()
        setTimeout(() => this.Loop(), this.LoopTime)
    }

    KillAds() {
        $('.promotedlink').css('display', 'none')
    }
}

/*
*
* MLIVE
*
*/

class MliveFixer extends Script {
    constructor() {
        super({
            name: 'Mlive',
            rules: {
                includes: ['mlive.com']
            }
        })

        this.loopTime = 500
    }

    Run() {
        this.HidePopups()
    }

    HidePopups() {
        try {
            const modal = $('.tp-modal')
            const content = $('.tp-backdrop')

            if (modal.length > 0 && content.length > 0) {
                Log('Removing modal popup')
                modal.remove()
                content.remove()
            }

            const header = $('h3').toArray().find((el) => $(el).text() === 'Disable your ad blocker to continue')
            if (header) {
                const overlay = $(header).parents().toArray().find((el) => $(el).css('background-color') === 'rgba(51, 51, 51, 0.5)')
                if (overlay) {
                    Log('Removing overlay popup')
                    $(overlay).remove()
                }
            }

            setTimeout(() => this.HidePopups(), this.loopTime)
        } catch (err) {
            Err('Error running Mlive Fixer')
            Err(err)
        }
    }
}

try {
    registry.RegisterScript(new SitefinityBar())
    registry.RegisterScript(new AmazonSmile())
    registry.RegisterScript(new AptivLinkFixer())
    registry.RegisterScript(new AptivCookieBanner())
    registry.RegisterScript(new BingFixer())
    registry.RegisterScript(new FacebookFixer())
    registry.RegisterScript(new SiteInfo())
    registry.RegisterScript(new Powermapper())
    registry.RegisterScript(new YouTube())
    registry.RegisterScript(new Reddit())
    registry.RegisterScript(new MliveFixer())
} catch (err) {
    Err('Error registering scripts.')
    Err(err)
}

class ParamRegistry {
    constructor() {
        this.Scripts = []
        this.ActiveScripts = []
    }

    RunScripts() {
        if (this.ActiveScripts.length === 0) return

        const names = this.ActiveScripts.map(s => `${s.param.Name}=${s.value}`)
        Log(`Params ${names.join(', ')}`)
        this.ActiveScripts.forEach(s => {
            try {
                s.param.Run(s.value)
            } catch (err) {
                Err(`Error running param ${s.param.Name}.`)
                Err(err)
            }
        })
    }

    RegisterScript(param) {
        this.Scripts.push(param)

        let match = false
        if (!param.Name || window.location.search.indexOf(param.Name) > -1 || window.location.hash.indexOf(param.Name) > -1) match = true

        if (match) {
            let value = GetQueryVariable(param.Name)
            if (!value) value = param.Default
            this.ActiveScripts.push({ param, value })
        }
    }
}

const paramRegistry = new ParamRegistry()

class Param {
    constructor({ name, defaultValue }) {
        this.Name = null
        this.Default = null
        if (name) this.Name = name
        if (defaultValue) this.Default = defaultValue
    }
}

class Color extends Param {
    constructor(data) {
        super(data)

        this.Filter = null
        this.Postfix = null
        this.Ranged = false

        const { filter, postfix, ranged } = data
        if (filter) this.Filter = filter
        if (postfix) this.Postfix = postfix

        if (ranged) {
            this.Ranged = true
            this.Postfix = '%'
        }
    }

    Run(amount) {
        if (this.Ranged) {
            amount = parseInt(amount, 10)
            amount += 100
            amount = amount.toString()
        }

        amount += this.Postfix

        let existing = $('body').css('filter') || ''
        if (existing.toLowerCase() === 'none') existing = ''
        if (existing) existing += ' '

        const val = `${existing}${this.Filter}(${amount})`
        $('body').css('filter', val)
    }
}

class Gray extends Color {
    constructor() {
        super({
            name: 'colors.gray',
            filter: 'grayscale',
            postfix: '%',
            defaultValue: '100'
        })
    }
}

class Invert extends Color {
    constructor() {
        super({
            name: 'colors.invert',
            filter: 'invert',
            postfix: '%',
            defaultValue: '100'
        })
    }
}

class Contrast extends Color {
    constructor() {
        super({
            name: 'colors.cont',
            filter: 'contrast',
            ranged: true,
            defaultValue: '100'
        })
    }
}

class Brightness extends Color {
    constructor() {
        super({
            name: 'colors.bright',
            filter: 'brightness',
            ranged: true,
            defaultValue: '100'
        })
    }
}

class Saturate extends Color {
    constructor() {
        super({
            name: 'colors.sat',
            filter: 'saturate',
            ranged: true,
            defaultValue: '100'
        })
    }
}

class Hue extends Color {
    constructor() {
        super({
            name: 'colors.hue',
            filter: 'hue-rotate',
            postfix: 'deg',
            defaultValue: '90'
        })
    }
}

class FlipX extends Param {
    constructor() {
        super({ name: 'flip.x' })
    }

    Run() {
        let existing = $('body').css('transform') || ''
        if (existing.toLowerCase() === 'none') existing = ''
        if (existing) existing += ' '

        const val = `${existing}scaleX(-1)`
        $('body').css('transform', val)
    }
}

class FlipY extends Param {
    constructor() {
        super({ name: 'flip.y' })
    }

    Run() {
        let existing = $('body').css('transform') || ''
        if (existing.toLowerCase() === 'none') existing = ''
        if (existing) existing += ' '

        const val = `${existing}scaleY(-1)`
        $('body').css('transform', val)
    }
}

class TeamworkBullet extends Param {
    constructor() {
        super({ name: 'bullet.list' })
    }

    Run() {
        if ($('.board-column').length <= 0) {
            setTimeout(() => this.Run(), 1000)
        } else {
            setTimeout(() => this.CreateLists(), 5000)
        }
    }

    CreateLists() {
        let lists = ''

        $('.board-column').each((idx, col) => {
            try {
                const $col = $(col)
                const title = $col.find('.boards-column--header-title').text().trim()

                let names = ''
                $col.find('.task-card .name').each((idx, name) => {
                    const $name = $(name)
                    const text = $name.text().trim()
                    if (text) names += `<li>${text}</li>`
                })

                if (names) lists += `<div><h2>${title}</h2><ul>${names}</ul><div>`
            } catch (err) {
                console.error(err)
            }

        })

        $('#mainContent').prepend(`<div id="st-lists">${lists}</div>`)
    }
}

class TeamworkTable extends Param {
    constructor() {
        super({ name: 'table.list' })
    }

    Run() {
        if ($('.board-column').length <= 0) {
            setTimeout(() => this.Run(), 1000)
        } else {
            setTimeout(() => this.CreateLists(), 5000)
        }
    }

    CreateLists() {
        let lists = ''

        $('.board-column').each((idx, col) => {
            try {
                const $col = $(col)
                const title = $col.find('.boards-column--header-title').text().trim()

                let names = ''
                $col.find('.task-card').each((idx, task) => {
                    const $task = $(task)
                    const $name = $task.find('.name')
                    const $estimate = $task.find('[aria-label="Estimated Time"]')
                    let estimate = $estimate.text().trim()
                    if (estimate) estimate = estimate.match(/\d+/g).join('')
                    const text = $name.text().trim()
                    if (text) names += `<tr><td>${text}</td><td>${estimate}</td></tr>`
                })

                if (names) lists += `<div><h2>${title}</h2><table>${names}</table><div>`
            } catch (err) {
                console.error(err)
            }

        })

        $('#mainContent').prepend(`<div id="st-lists">${lists}</div>`)
    }
}

class AutoRefresh extends Param {
    constructor() {
        super({
            name: 'auto.refresh',
            defaultValue: '30'
        })
    }

    Run(value) {
        value = parseInt(value, 10)
        Log(`AutoRefresh will refresh the page in ${value} seconds...`)

        setTimeout(() => {
            window.location.reload()
        }, value * 1000)
    }
}

class AddJquery extends Param {
    constructor() {
        super({ name: 'add.jquery' })
    }

    Run() {
        $('body').append(`<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>`)
    }
}

class AddLodash extends Param {
    constructor() {
        super({ name: 'add.lodash' })
    }

    Run() {
        $('body').append(`<script
        src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"
        crossorigin="anonymous"></script>`)
    }
}

class AddUnderscore extends Param {
    constructor() {
        super({ name: 'add.underscore' })
    }

    Run() {
        $('body').append(`<script
        src="https://cdn.jsdelivr.net/npm/underscore@1.13.1/underscore-umd-min.js"
        crossorigin="anonymous"></script>`)
    }
}

try {
    paramRegistry.RegisterScript(new Gray())
    paramRegistry.RegisterScript(new Invert())
    paramRegistry.RegisterScript(new Contrast())
    paramRegistry.RegisterScript(new Brightness())
    paramRegistry.RegisterScript(new Saturate())
    paramRegistry.RegisterScript(new Hue())
    paramRegistry.RegisterScript(new FlipX())
    paramRegistry.RegisterScript(new FlipY())
    paramRegistry.RegisterScript(new TeamworkBullet())
    paramRegistry.RegisterScript(new TeamworkTable())
    paramRegistry.RegisterScript(new AutoRefresh())
    paramRegistry.RegisterScript(new AddJquery())
    paramRegistry.RegisterScript(new AddLodash())
    paramRegistry.RegisterScript(new AddUnderscore())
} catch (err) {
    Err('Error registering params.')
    Err(err)
}

function UrlUpdated() {
    try {
        registry.RunScripts()
        paramRegistry.RunScripts()
    } catch (err) {
        Err('Error running scripts.')
        Err(err)
    }
}

let previousUrl = null
function WatchUrl() {
    if (window.location.href !== previousUrl) {
        Log(`Url changed to ${window.location.href}`)
        UrlUpdated()
        previousUrl = window.location.href
    }

    setTimeout(() => WatchUrl(), WATCH_URL_INTERVAL)
}

$(() => WatchUrl())

function Log(text) {
    console.log(`OSHKOSH: ${text}`)
}

function Err(text) {
    Log('OSHKOSH ERROR')
    console.error(text)
}

function GetQueryVariable(variable) {
    try {
        let query = null

        if (window.location.search) {
            query = window.location.search.substring(1)
        } else if (window.location.hash) {
            query = window.location.hash
            if (query.indexOf('?') === -1) return null
            query = query.split('?')[1]
        }

        if (!query) return null

        let vars = query.split('&')
        for (let i = 0; i < vars.length; i++) {
            const pair = vars[i].split('=')
            if (pair.length === 1) continue
            if (decodeURIComponent(pair[0]) === variable) {
                return decodeURIComponent(pair[1])
            }
        }
    } catch (err) {
        console.error(err)
    }

    return null
}

function GetAriaLabel(elem) {
    const $elem = $(elem)
    const label = $elem.attr('aria-label')
    if (label) return label

    const id = $elem.attr('aria-labelledby')
    if (id) return $(`#${id}`).text()
}
