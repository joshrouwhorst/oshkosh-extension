const mainStyles = {
    padding: 'padding: 5px 0;',
    fontSize: 'font-size: 15px;',
    lineHeight: 'line-height: 20px;',
    display: 'display: inline-block;',
    backgroundColor: 'background-color: #000;',
    color: 'color: #FFF;',
    bold: 'font-weight: bold;'
}

const secondaryStyles = {
    ...mainStyles,
    color: 'color: #BBB;'
}

const errorStyles = {
    ...mainStyles,
    backgroundColor: 'background-color: #F00;'
}

setStyles(mainStyles)
setStyles(secondaryStyles)
setStyles(errorStyles)

function setStyles(styleSet) {
    const { display, padding, fontSize, lineHeight, color, backgroundColor } = styleSet
    styleSet.fontStyle = display + padding + fontSize + lineHeight
    styleSet.colorStyle = color + backgroundColor
    styleSet.style = styleSet.fontStyle + styleSet.colorStyle
}

// const fontStyles = 'font-size: 15px; padding: 5px 10px; line-height: 20px;'
// const colorStyle1 = 'display: inline-block; background-color: #000; color: #FFF;'
// const colorStyle2 = 'display: inline-block; background-color: #000; color: #BBB;'

function _base(text) {
    const { colorStyle, fontStyle } = mainStyles

    console.log(`%c${text}`,
        `${colorStyle} ${fontStyle}`)
}

function Obj(object) {
    console.log(object)
}

function Log(text) {
    const { style: mainStyle } = mainStyles
    const { style: secondStyle, bold } = secondaryStyles

    console.log(`%cOSH:%c ${text}`,
        `${secondStyle} ${bold}`,
        mainStyle)
}

function Err(text) {
    const { style, bold } = errorStyles
    console.error(`%cOSH ERROR:%c ${text}`,
        `${style} ${bold}`,
        style)
}

function Divider(opts) {
    const { style: mainStyle } = mainStyles
    const { style: secondStyle } = secondaryStyles

    const lineStyle = secondStyle
    const textStyle = mainStyle

    let { text, style } = opts || {}
    let line = null
    style = style || ''

    switch (style) {
        case 'line':
            line = '----------------------------------------'
            break
        case 'equal':
            line = '========================================'
            break
        default:
            line = '****************************************'
            break
    }

    if (!text) return console.log(`%c${line}`, lineStyle)

    const lineLength = line.length

    let lineText = line.substring(0, lineLength - (text.length + 2))
    lineText = lineText.substring(0, Math.ceil(lineText.length / 2))

    line = `%c${lineText}  %c${text}%c  ${lineText}`
    return console.log(line,
        lineStyle,
        textStyle,
        lineStyle)
}

function Box(opts) {
    const { style: mainStyle } = mainStyles
    const { style: secondStyle } = secondaryStyles

    if (opts.title) Divider({ text: opts.title })
    else Divider()

    opts.lines.forEach(line => {
        if (line.subhead) Divider({ text: line.text, style: 'equal' })
        else console.log(`%c**%c ${line.text}`, secondStyle, mainStyle)
    })

    if (opts.outro) Divider({ text: opts.outro })
    else Divider()
}

module.exports = {
    Obj,
    Log,
    Err,
    Divider,
    Box
}