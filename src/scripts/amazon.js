const { Err, Log } = require('../utils')
const Script = require('../models/script')

class AmazonSmile extends Script {
    constructor () {
        super({
            name: 'Amazon Smile',
            rules: {
                includes: [
                    'amazon.com'
                ]
            }
        })
    }

    Run () {
        try {
            const here = new URL(window.location.href)
            if (here.hostname === 'www.amazon.com') {
                const next = new URL(here.href.replace(here.hostname, 'smile.amazon.com'))
                Log(`Redirecting from: ${window.location.href}\nTo:${next.href}`)
                window.location.href = next.href
            }
        } catch (err) {
            Err('Amazon Smile - Run Err')
            Err(err)
        }
    }
}

module.exports = {
    AmazonSmile
}
