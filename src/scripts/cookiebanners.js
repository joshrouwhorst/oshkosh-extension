const { $, Log } = require('../utils')
const Script = require('../models/script')

const BANNER_SELECTORS = [
    '#onetrust-consent-sdk',
    '.c-cookie-banner',
    '.js-consent-banner'
]

class CookieBanners extends Script {
    constructor() {
        super({
            name: 'Cookie Banners'
        })

        this.LoopTime = 500
        this.MaxLoops = 40
    }

    Run() {
        this.RemoveBanners()
    }

    RemoveBanners(loop) {
        if (!loop) loop = 1

        BANNER_SELECTORS.forEach((selector) => {
            if ($(selector).length > 0) {
                Log(`Removing cookie banner with selector ${selector}`)
                $(selector).remove()
            }
        })

        if (loop < this.MaxLoops) setTimeout(() => this.RemoveBanners(loop + 1), this.LoopTime)
    }
}

module.exports = {
    CookieBanners
}
