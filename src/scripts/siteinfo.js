const { Box } = require('../utils')
const Script = require('../models/script')

class SiteInfo extends Script {
    constructor() {
        super({
            name: 'Site Info'
        })
    }

    Run() {
        const metas = []
        const openGraph = []
        const twitter = []
        let charset = null

        const boxOpts = {
            title: 'SITE INFO',
            lines: []
        }

        const metaTags = document.getElementsByTagName('meta')

        for (let i = 0; i < metaTags.length; i++) {
            const tag = metaTags[i]
            const { name, content, property, charset: char } = tag.attributes
            const isTwitter = name ? name.value.includes('twitter:') : false
            const isOg = property ? property.value.includes('og:') : false

            if (isTwitter && name && content) twitter.push({ name: name.value, content: content.value })
            else if (isOg && property && content) openGraph.push({ property: property.value, content: content.value })
            else if (name && content) metas.push({ name: name.value, content: content.value })

            if (char) charset = char.value
        }

        if (charset) boxOpts.lines.push({ text: `Charset: ${charset}` })

        if (metas.length > 0) boxOpts.lines.push({ text: 'Meta', subhead: true })

        for (let i = 0; i < metas.length; i++) {
            const { name, content } = metas[i]
            boxOpts.lines.push({ text: `${name}:    ${content}` })
        }

        if (openGraph.length > 0) boxOpts.lines.push({ text: 'OpenGraph', subhead: true })

        for (let i = 0; i < openGraph.length; i++) {
            const { property, content } = openGraph[i]
            boxOpts.lines.push({ text: `${property}:    ${content}` })
        }

        if (twitter.length > 0) boxOpts.lines.push({ text: 'Twitter', subhead: true })

        for (let i = 0; i < twitter.length; i++) {
            const { name, content } = twitter[i]
            boxOpts.lines.push({ text: `${name}:    ${content}` })
        }

        boxOpts.lines.push({ text: '' })

        Box(boxOpts)
    }
}

module.exports = {
    SiteInfo
}
