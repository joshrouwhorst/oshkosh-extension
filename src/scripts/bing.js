const { $ } = require('../utils')
const Script = require('../models/script')

class BingFixer extends Script {
    constructor() {
        super({
            name: 'Bing Fixer',
            rules: {
                includes: [
                    'bing.com'
                ]
            }
        })
    }

    Run() {
        this.ClosePopup()
        this.RemoveSaveOption()
    }

    ClosePopup() {
        const selector = '#signout_closebtn'
        $(selector).click()
        setTimeout(() => this.ClosePopup(), 1000)
    }

    RemoveSaveOption() {
        $('[aria-label="Save Control"]').remove()
        setTimeout(() => this.RemoveSaveOption(), 1000)
    }
}

module.exports = {
    BingFixer
}
