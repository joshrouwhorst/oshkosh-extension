const { $ } = require('../utils')
const Script = require('../models/script')

class Reddit extends Script {
    constructor() {
        super({
            name: 'Reddit',
            rules: {
                includes: ['reddit.com']
            }
        })

        this.LoopTime = 500
        this.Tries = 0
        this.MaxTries = 10
    }

    Run() {
        this.Loop()
    }

    Loop() {
        this.KillAds()
        setTimeout(() => this.Loop(), this.LoopTime)
    }

    KillAds() {
        $('.promotedlink').css('display', 'none')
    }
}

module.exports = {
    Reddit
}
