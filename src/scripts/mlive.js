const { $, Log, Err } = require('../utils')
const Script = require('../models/script')

class MliveFixer extends Script {
    constructor() {
        super({
            name: 'Mlive',
            rules: {
                includes: ['mlive.com']
            }
        })

        this.loopTime = 500
    }

    Run() {
        this.HidePopups()
    }

    HidePopups() {
        try {
            const modal = $('.tp-modal')
            const content = $('.tp-backdrop')

            if (modal.length > 0 && content.length > 0) {
                Log('Removing modal popup')
                modal.remove()
                content.remove()
            }

            const header = $('h3').toArray().find((el) => $(el).text() === 'Disable your ad blocker to continue')
            if (header) {
                const overlay = $(header).parents().toArray().find((el) => $(el).css('background-color') === 'rgba(51, 51, 51, 0.5)')
                if (overlay) {
                    Log('Removing overlay popup')
                    $(overlay).remove()
                }
            }

            setTimeout(() => this.HidePopups(), this.loopTime)
        } catch (err) {
            Err('Error running Mlive Fixer')
            Err(err)
        }
    }
}

module.exports = {
    MliveFixer
}
