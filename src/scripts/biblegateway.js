const { $, Log, HasParam } = require('../utils')
const Script = require('../models/script')

class BibleGateway extends Script {
    constructor () {
        super({
            name: 'Bible Gateway',
            rules: {
                includes: [
                    'biblegateway.com/passage/'
                ]
            },
            help: 'Bible Gateway - You can use the "osh.strip" query param to strip verses and footnote refs.'
        })
    }

    Run () {
        $('.resources').hide()

        const strip = () => {
            $('sup.footnote').each((idx, item) => {
                $(item).remove();
            })

            $('sup.versenum').each((idx, item) => {
                $(item).remove();
            })

            $('span.chapternum').each((idx, item) => {
                $(item).remove()
            })
        }

        if (HasParam('osh.strip')) {
            strip();

            const nexthref = $('.next-chapter').attr('href')
            $('.next-chapter').attr('href', `${nexthref}&osh.strip`)

            const prevhref = $('.prev-chapter').attr('href')
            $('.prev-chapter').attr('href', `${prevhref}&osh.strip`)
        } else if (HasParam('osh.mark')) {
            const now = new Date()

            $('.std-text').prepend(`<br /><div># ${$('.quick-search').val()}</div><div>Added: ${now.toUTCString()}</div><div>Tags: [[Bible Chapter]] [[Bible]]</div><br /><div>[Web](${window.location.href})</div>`)

            $('sup.footnote').each((idx, item) => {
                const text = $(item).text().trim()
                $(item).replaceWith('**^** ')
            })

            $('sup.versenum').each((idx, item) => {
                const text = $(item).text().trim()
                $(item).replaceWith(`*${text}* `)
            })

            $('span.chapternum').each((idx, item) => {
                const text = $(item).text().trim()
                $(item).replaceWith(`**${text}** `)
            })

            const nexthref = $('.next-chapter').attr('href')
            $('.next-chapter').attr('href', `${nexthref}&osh.mark`)

            const prevhref = $('.prev-chapter').attr('href')
            $('.prev-chapter').attr('href', `${prevhref}&osh.mark`)
        } else if (HasParam('osh.quote')) {
            strip();

            $('.passage-text p:not(.spacer)').after('<p class="spacer"></p>')
            $('.passage-text p').prepend('> ')

            const nexthref = $('.next-chapter').attr('href')
            $('.next-chapter').attr('href', `${nexthref}&osh.quote`)

            const prevhref = $('.prev-chapter').attr('href')
            $('.prev-chapter').attr('href', `${prevhref}&osh.quote`)
        }

        const params = ['osh.strip', 'osh.mark', 'osh.quote']

        const buildUrl = (param) => {
            var loc = window.location.href;
            params.forEach(p => { loc = loc.replace(`&${p}`, '') })
            if (param) loc += `&${param}`
            return loc;
        }

        $('.std-text, .passage-text').prepend(`<div><a href="${buildUrl()}">Regular</a> | <a href="${buildUrl('osh.strip')}">Strip</a> | <a href="${buildUrl('osh.quote')}">Quote</a> | <a href="${buildUrl('osh.mark')}">Markdown</a></div>`)
    }
}

class BibleGatewayAds extends Script {
    constructor () {
        super({
            name: 'Bible Gateway Ads',
            rules: {
                includes: [
                    'biblegateway.com'
                ]
            },
            help: 'Bible Gateway Ads - Looks for ad containers and automatically removes them.'
        })
    }

    Run () {
        this.RemoveAds()
    }

    RemoveAds () {
        $('.bottom-ad').remove()
        $('[aria-label="advertisement"]').remove()
        $('.top-wrapper').remove()
        setTimeout(() => this.RemoveAds(), 1000)
    }
}

module.exports = {
    BibleGateway,
    BibleGatewayAds
}
