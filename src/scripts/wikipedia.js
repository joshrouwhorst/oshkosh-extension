const { $, Log, HasParam } = require('../utils')
const Script = require('../models/script')

class Wikipedia extends Script {
    constructor() {
        super({
            name: 'Wikipedia',
            rules: {
                includes: [
                    'wikipedia.org/wiki/'
                ]
            },
            help: 'Wikipedia - Adds options to remove links and such for easy copying.'
        })
    }

    Run() {
        const url = window.location.href
        let normalUrl
        let stripUrl

        if (url.includes('&osh.strip')) {
            normalUrl = url.replace('&osh.strip', '')
            stripUrl = url
        } else if (url.includes('?osh.strip')) {
            normalUrl = url.replace('?osh.strip', '')
            stripUrl = url
        } else if (url.includes('?')) {
            normalUrl = url
            stripUrl = url + '&osh.strip'
        } else {
            normalUrl = url
            stripUrl = url + '?osh.strip'
        }

        $('#right-navigation .vector-menu-content-list').prepend(`<li class="mw-list-item collapsible"><a href="${normalUrl}"><span>Normal</span></a></li>`)
        $('#right-navigation .vector-menu-content-list').prepend(`<li class="mw-list-item collapsible"><a href="${stripUrl}"><span>Strip</span></a></li>`)

        if (HasParam('osh.strip')) {
            $('#bodyContent sup').remove()

            $('#bodyContent a').each((idx, item) => {
                const $item = $(item)
                $item.replaceWith(`<span>${$item.html()}</span>`)
            })
        }
    }
}

module.exports = {
    Wikipedia
}
