const { $, Log, Err, Obj } = require('../utils')
const Script = require('../models/script')

class YouTube extends Script {
    constructor() {
        super({
            name: 'YouTube',
            rules: {
                includes: ['youtube.com']
            }
        })

        this.LoopTime = 500
        this.MaxTries = 10
    }

    Run() {
        if (window.location.pathname.toLowerCase().includes('/watch')) {
            this.HideChat()
            this.HideComments()
            //this.ShowDescription()
            this.CloseAds()
            this.AutoSkip()
        }
    }

    HidePremiumPopup() {
        try {
            if ($('#dismiss-button.ytd-mealbar-promo-renderer .ytd-button-renderer').length > 0) {
                Log('Hiding Premium Popup')
                $('#dismiss-button.ytd-mealbar-promo-renderer .ytd-button-renderer').trigger('click')
            }
            setTimeout(() => this.HidePremiumPopup(), this.LoopTime)
        } catch (err) {
            Err('Error running to YouTube Hide Premium Popup')
            Obj(err)
        }
    }

    HideChat(tries) {
        try {
            if (!tries) tries = 0

            const $elem = $('#show-hide-button a')
            if ($elem.length <= 0 && tries < this.MaxTries) return setTimeout(() => this.HideChat(tries), this.LoopTime)
            if ($elem.length > 0) {
                Log('YouTube Chat Collapsed')
                return $elem[0].trigger('click')
            }

            setTimeout(() => this.HideChat(tries + 1), this.LoopTime)
        } catch (err) {
            Err('Error running YouTube Chat Collapse')
            Obj(err)
        }
    }

    HidePopup(tries) {
        try {
            if (!tries) tries = 0

            if ($('ytd-mealbar-promo-renderer #dismiss-button').length <= 0 && tries < this.MaxTries) {
                setTimeout(() => this.HidePopup(tries + 1), this.LoopTime)
            }

            if ($('ytd-mealbar-promo-renderer #dismiss-button').length > 0) {
                $('ytd-mealbar-promo-renderer #dismiss-button').trigger('click')
                Log('YouTube popup exited')
            }
        } catch (err) {
            Err('Error trying to hide popup')
            Obj(err)
        }
    }

    HideComments(tries) {
        try {
            if (!tries) tries = 0

            if (window.location.href.indexOf('show.comments') > -1) return

            if ($('#comments').length === 0 && tries < this.MaxTries) {
                return setTimeout(() => this.HideComments(tries + 1), this.LoopTime)
            }

            if ($('#comments').length > 0) {
                Log('Hiding Comments')
                $('#comments').hide()
            }
        } catch (err) {
            Err('Error running YouTube Hide Comments')
            Obj(err)
        }
    }

    ShowDescription(tries) {
        try {

            if (!tries) tries = 0

            if ($('.more-button').length === 0 && tries < this.MaxTries) {
                return setTimeout(() => this.ShowDescription(tries + 1), this.LoopTime)
            }

            if ($('.more-button').length > 0) {
                Log('Showing Description')
                $('.more-button').trigger('click')
            }

            // When switching routes, it might pick up on the previous more-button, so keep trying.
            if (tries < this.MaxTries) {
                return setTimeout(() => this.ShowDescription(tries + 1), this.LoopTime)
            }
        } catch (err) {
            Err('Error running YouTube Show Description')
            Obj(err)
        }
    }

    CloseAds() {
        try {

            if ($('.ytp-ad-overlay-close-container').length > 0) {
                Log('Closing video banner ad')
                $('.ytp-ad-overlay-close-container').trigger('click')
            }

            if ($('.ytp-ad-overlay-close-button').length > 0) {
                Log('Closing video banner ad')
                $('.ytp-ad-overlay-close-button').trigger('click')
            }

            if ($('ytd-promoted-sparkles-web-renderer').length > 0) {
                Log('Closing side ad')
                $('ytd-promoted-sparkles-web-renderer').remove()
            }

            setTimeout(() => this.CloseAds(), this.LoopTime)
        } catch (err) {
            Err('Error running YouTube Close Ad')
            Obj(err)
        }
    }

    AutoSkip() {
        try {
            if ($('.ytp-ad-skip-button-text').length > 0) {
                Log('Auto Skipping')
                $('.ytp-ad-skip-button-text').trigger('click')
            }
            setTimeout(() => this.AutoSkip(), this.LoopTime)
        } catch (err) {
            Err('Error running YouTube AutoSkip')
            Obj(err)
        }
    }
}

module.exports = {
    YouTube
}
