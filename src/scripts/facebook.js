const { $, Log, Err } = require('../utils')
const Script = require('../models/script')

class FacebookFixer extends Script {
    constructor() {
        super({
            name: 'Facebook Fixer',
            rules: {
                includes: [
                    'facebook.com'
                ]
            }
        })

        this.LoopTime = 500
        this.AdsRemoved = 0
        this.Settings = {}
        this.LocalStorageName = 'FacebookFixerSettings'

        this.GetSettings()
    }

    Run() {
        this.SetupMain()
        this.Loop()
    }

    SetupMain() {
        const elem = $('div[role="main"]')
        if (elem.length <= 0) return Log('Did not find a Main element')

        if (this.Settings.HideMain) {
            const button = $('<button style="margin: 25px auto; padding: 25px; font-size: 1.3em;">Show Main</button>')
            button.on('click', () => {
                this.Settings.HideMain = false
                this.SaveSettings()
                window.location.reload()
            })
            elem.remove()
            button.prependTo('div[data-pagelet="LeftRail"]')
        } else {
            const button = $('<button style="margin: 25px auto; padding: 25px; font-size: 1.3em;">Hide Main</button>')
            button.on('click', () => {
                button.remove()
                this.Settings.HideMain = true
                this.SaveSettings()
                this.SetupMain()
            })
            button.prependTo('div[data-pagelet="LeftRail"]')
        }
    }

    Loop() {
        this.CheckFeed()
        this.AdLinks()
        this.RightRail()
        setTimeout(() => this.Loop(), this.LoopTime)
    }

    CheckFeed() {
        try {
            const elems = $('a[aria-label="Sponsored"],div[aria-label="Sponsored"]').closest('div[role="article"]')
            elems.replaceWith('<div>Ad Removed</div>')
            if (elems.length > 0) this.AdsRemoved += elems.length
        } catch (err) {
            Err('Facebook Fixer - Check Feed Err')
            Err(err)
        }
    }

    AdLinks() {
        try {
            const elems = $('a[aria-label="Advertiser link"]')
            elems.replaceWith('<div>Ad Removed</div>')
            if (elems.length > 0) this.AdsRemoved += elems.length
        } catch (err) {
            Err('Facebook Fixer - Check Feed Err')
            Err(err)
        }
    }

    RightRail() {
        const elem = $('div[data-pagelet="RightRail"]:not([data-fixed="true"])')
        if (elem.length <= 0) return
        elem.closest('div[role="complementary"]').remove()
        elem.attr('data-fixed', 'true')
    }

    GetSettings() {
        const settings = localStorage.getItem(this.LocalStorageName)
        if (settings) this.Settings = JSON.parse(settings)
    }

    SaveSettings() {
        const settings = JSON.stringify(this.Settings)
        localStorage.setItem(this.LocalStorageName, settings)
    }
}

module.exports = {
    FacebookFixer
}
