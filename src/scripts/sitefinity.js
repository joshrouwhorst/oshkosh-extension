const { $, Err, Obj } = require('../utils')
const Script = require('../models/script')

class SitefinityBar extends Script {
    constructor() {
        super({
            name: 'Sitefinity Bar',
            rules: {
                includes: [
                    '/sitefinity',
                    '/Sitefinity'
                ]
            }
        })

        this.Links = [
            {
                name: 'Pages',
                link: '/Sitefinity/Pages',
            },
            {
                name: 'Images',
                link: '/Sitefinity/Content/Images'
            },
            {
                name: 'Documents',
                link: '/Sitefinity/Content/Documents'
            },
            {
                name: 'Labels',
                link: '/Sitefinity/Administration/Labels'
            },
            {
                name: 'Advanced Settings',
                link: '/Sitefinity/Administration/Settings/Advanced'
            },
            {
                name: 'Translations Settings',
                link: '/Sitefinity/Administration/Settings/Advanced/Translations'
            }
        ]
    }
    // PUT: /RestApi/adminapp/toggle-backend-per-user
    // GET: /RestApi/adminapp/backend-status

    Run() {
        if ($('#oshkosh-sf-bar').length > 0) return
        this.Update()
    }

    async Update() {
        const linkStyle = 'style="display: inline-block; color: white; padding: 5px 0 5px 20px;"'
        const spanStyle = 'style="display: inline-block; color: #BBB; padding: 5px 0 5px 20px;"'

        if ($('#oshkosh-sf-bar').length <= 0) {
            $('body').prepend(`<div id="oshkosh-sf-bar" style="background-color: black;"></div>`)
            this.bar = $('#oshkosh-sf-bar')
        }

        let version = null
        $('meta[name]').each((idx, m) => {
            const $tag = $(m)
            if ($tag.attr('name').toLowerCase() === 'generator') {
                version = $tag.attr('content')
            }
        })

        if (version) version = `<span ${spanStyle}>${version}</span>`
        else version = ''

        let links = ''

        for (let i = 0; i < this.Links.length; i++) {
            let link = this.Links[i]
            links += `<a ${linkStyle} href="${link.link}">${link.name}</a>`
        }

        this.bar.html(`${version} ${links}`)
    }
}

module.exports = {
    SitefinityBar
}
