const { $, Log, Err } = require('../utils')
const Script = require('../models/script')

class Powermapper extends Script {
    constructor() {
        super({
            name: 'Powermapper Reports',
            rules: {
                includes: ['powermapper.com']
            }
        })
    }

    Run() {
        if (window.location.pathname === '/') {
            this.HandleHomepage()
        } else if (window.location.pathname.toLowerCase().indexOf('/scans/history') > -1) {
            this.HandleScanHistory()
        }
    }

    HandleHomepage() {
        $('tbody > tr > td > a').each((idx, item) => {
            const $item = $(item)
            const customer = $item.text()

            const url = this.CheckLink($item.attr('href'))
            fetch(url).then(resp => resp.text()).then((data) => {
                const $page = $(data)
                let gotFirst = false
                Log(`Got scan page for ${customer}`)

                // Getting the report
                try {
                    $page.find('tbody > tr > td > a').each((idx, item) => {
                        if (gotFirst) return
                        gotFirst = true
                        const $scanItem = $(item)
                        const url = this.CheckLink($scanItem.attr('href'))
                        fetch(url).then(resp => resp.text()).then((data) => {
                            const $nextPage = $(data)
                            const link = $nextPage.find('#SelectExportFormat>option:contains("Issue Report (Excel CSV)")').attr('value')
                            $item.after('<span> - <a href="' + link + '">Report</a></span>')
                        }).catch((err) => {
                            Err('Error trying to get report page.')
                            Err(err)
                        })
                    })
                } catch (err) {
                    Err(`Error trying to get report for ${customer}`)
                    Err(err)
                }

                try {
                    Log(`Looking for delete page for ${customer}`)
                    const linkarr = $page.find('a').toArray()
                    const deleteLink = linkarr.find(link => link.href && link.href.indexOf && link.href.indexOf('/Scans/Delete') > -1)
                    if (deleteLink) {
                        const url = this.CheckLink(deleteLink.href)
                        fetch(url).then(resp => resp.text()).then((data) => {
                            const $delPage = $(data)
                            const $form = $delPage.find('form')
                            if (!$form || $form.length === 0) return
                            const formAction = $form.attr('action')
                            const scanIds = $delPage.find('input[name="ProcessIds"]').toArray().map(c => c.value)
                            const token = $delPage.find('input[name="__RequestVerificationToken"]').attr('value')

                            if (scanIds.length <= 1) return
                            const deleteIds = scanIds.slice(1, scanIds.length)
                            const $link = $('<button>Delete Old Scans</button>')
                            $link.on('click', () => this.DeleteOldScans(deleteIds, formAction, token, $link))
                            $item.after($link)
                        }).catch((err) => {
                            Err('Error collecting delete page.')
                            Err(err)
                        })
                    }
                } catch (err) {
                    Err(`Error trying to get delete scan ids for ${customer}`)
                    Err(err)
                }
            }).catch((err) => {
                Err(`Error trying to go to scan page.`)
                Err(err)
            })
        })
    }

    HandleScanHistory() {
        $('tbody > tr > td > a').each((idx, item) => {
            const $item = $(item)
            const url = this.CheckLink($item.attr('href'))
            fetch(url).then(resp => resp.text()).then((data) => {
                const $page = $(data)
                const link = $page.find('#SelectExportFormat>option:contains("Issue Report (Excel CSV)")').attr('value')
                $item.after('<span> - <a href="' + link + '">Report</a></span>')
            }).catch((err) => {
                Err('Error getting scan history')
                Err(err)
            })
        })
    }

    DeleteOldScans(ids, formAction, token, $link) {
        if (!confirm(`Are you sure you want to delete ${ids.length} old scan(s)?`)) return

        const url = this.CheckLink(formAction)
        fetch(url, { method: 'POST', data: { ProcessIds: ids, __RequestVerificationToken: token } }).then((data) => {
            $link.text('Success')
        }).catch((err) => {
            $link.text('Error')
            Err('Error trying to delete scans.')
            Err(err)
        })
    }

    CheckLink(href) {
        if (href.indexOf(window.location.origin) !== 0) {
            return window.location.origin + href
        } else {
            return href
        }
    }
}

module.exports = {
    Powermapper
}
