const { $, Log, Err, Obj } = require('../utils')
const Script = require('../models/script')

class Twitter extends Script {
    constructor() {
        super({
            name: 'Twitter',
            rules: {
                includes: ['twitter.com']
            }
        })

        this.LoopTime = 500
        this.MaxTries = 10
    }

    Run() {
        this.HideAds()
    }

    HideAds() {
        try {
            $('span:not(.osh-checked)').each((idx, item) => {
                const $item = $(item)
                const text = $item.text().trim().toLowerCase()
                if (text === 'promoted' || text.indexOf('promoted by') === 0) {
                    $item.parent().parent().parent().parent().parent().remove()
                } else {
                    $item.addClass('osh-checked')
                }
            })

            setTimeout(() => this.HideAds(), this.LoopTime)
        } catch (err) {
            Err('Error running to Twitter Hide Ads')
            Obj(err)
        }
    }
}

module.exports = {
    Twitter
}
