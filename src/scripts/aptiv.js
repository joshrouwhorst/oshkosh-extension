const { $ } = require('../utils')
const Script = require('../models/script')

class AptivLinkFixer extends Script {
    constructor() {
        super({
            name: 'Aptiv Link Fixer',
            rules: {
                includes: [
                    'aptiv.springthroughtest.com',
                    'aptivtest.azurewebsites.net',
                    'origin.aptivstaging.com',
                    'aptivdev.azurewebsites.net',
                    'aptivdotcom-us-central-staging.azurewebsites.net',
                    'localhost'
                ]
            }
        })

        this.Host = window.location.host
    }

    Run() {
        $(() => {
            const links = $('a[href]')
            for (var i = 0; i < links.length; i++) {
                while (links[i].href.indexOf('www.aptiv.com') > -1) {
                    links[i].href = links[i].href.replace('www.aptiv.com', this.Host)
                }
            }
        })
    }
}

module.exports = {
    AptivLinkFixer
}
