const { DateTime } = require('luxon')
const localForage = require('localforage')
const { Log, Err, GIT_BASE, HasParam } = require('./utils')

try {

    (() => {
        // eslint-disable-next-line
        if (HasParam('oshdebug')) debugger
    })()

    init();

    async function init() {
        Log('Initializing')
        const code = await localForage.getItem('code')

        if (!code || !code.script) {
            Log('No existing code found. Fetching.')
            const script = await update()
            if (script) {
                Log('Code fetched. Running.')
                eval(script)
            } else {
                Log('Code could not be fetched. Ending.')
            }
            return
        }

        Log('Running.')
        eval(code.script)

        if (HasParam('oshupdate') || DateTime.now() > DateTime.fromISO(code.expiration)) {
            Log('Updating.')
            await update()
            Log('Update complete.')
        }

        Log('Initializing complete.')
    }

    async function update() {
        const script = await getFreshScript()
        if (!script) return null

        const code = {
            expiration: DateTime.now().plus({ days: 1 }).toISO(),
            script
        }

        await localForage.setItem('code', code)

        return script
    }

    async function getFreshScript() {
        try {
            const resp = await fetch(`${GIT_BASE}/extension/oshkosh.js`)
            if (resp.ok) {
                const js = await resp.text()
                return js
            }
            return null
        } catch (err) {
            Err(err)
        }
    }
} catch (err) {
    Err(err)
}