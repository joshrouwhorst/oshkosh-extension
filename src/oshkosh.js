const { $, Log, Err, Box, HasParam } = require('./utils')

if (HasParam('oshstop')) return

const { ScriptRegistry } = require('./models/scriptregistry')
const { ParamRegistry } = require('./models/paramregistry')

/*
*
* Scripts
*
*/
// Amazon Smile has officially been retired as of 2023-02-20.
//const { AmazonSmile } = require('./scripts/amazon')

const { AptivLinkFixer } = require('./scripts/aptiv')
const { BibleGatewayAds, BibleGateway } = require('./scripts/biblegateway')
const { BingFixer } = require('./scripts/bing')
const { CookieBanners } = require('./scripts/cookiebanners')
const { FacebookFixer } = require('./scripts/facebook')
const { MliveFixer } = require('./scripts/mlive')
const { Powermapper } = require('./scripts/powermapper')
const { Reddit } = require('./scripts/reddit')
const { SitefinityBar } = require('./scripts/sitefinity')
const { SiteInfo } = require('./scripts/siteinfo')
const { Twitter } = require('./scripts/twitter')
const { Wikipedia } = require('./scripts/wikipedia')
const { YouTube } = require('./scripts/youtube')

/*
*
* Params
*
*/
const { Gray, Invert, Contrast, Brightness, Saturate, Hue } = require('./params/colors')
const { FlipX, FlipY } = require('./params/flips')
const { TeamworkBullet, TeamworkTable } = require('./params/teamwork')
const { AutoRefresh } = require('./params/autorefresh')
const { AddJquery, AddLodash, AddUnderscore } = require('./params/libraries')
const { WikiCopy } = require('./params/wikipedia')
const { OshTest } = require('./params/test')

// eslint-disable-next-line
if (HasParam('oshdebug')) debugger

const WATCH_URL_INTERVAL = 500;

const scriptRegistry = new ScriptRegistry()

// TODO - Figure out try catch for the independent scripts.

try {
    scriptRegistry.RegisterScript(new AptivLinkFixer())
    scriptRegistry.RegisterScript(new BibleGateway())
    scriptRegistry.RegisterScript(new BibleGatewayAds())
    scriptRegistry.RegisterScript(new BingFixer())
    scriptRegistry.RegisterScript(new CookieBanners())
    scriptRegistry.RegisterScript(new FacebookFixer())
    scriptRegistry.RegisterScript(new MliveFixer())
    scriptRegistry.RegisterScript(new Powermapper())
    scriptRegistry.RegisterScript(new Reddit())
    scriptRegistry.RegisterScript(new SitefinityBar())
    scriptRegistry.RegisterScript(new SiteInfo())
    scriptRegistry.RegisterScript(new Twitter())
    scriptRegistry.RegisterScript(new Wikipedia())
    scriptRegistry.RegisterScript(new YouTube())
} catch (err) {
    Err('Error registering scripts.')
    Err(err)
}

const paramRegistry = new ParamRegistry()

try {
    paramRegistry.RegisterScript(new Gray())
    paramRegistry.RegisterScript(new Invert())
    paramRegistry.RegisterScript(new Contrast())
    paramRegistry.RegisterScript(new Brightness())
    paramRegistry.RegisterScript(new Saturate())
    paramRegistry.RegisterScript(new Hue())
    paramRegistry.RegisterScript(new FlipX())
    paramRegistry.RegisterScript(new FlipY())
    paramRegistry.RegisterScript(new TeamworkBullet())
    paramRegistry.RegisterScript(new TeamworkTable())
    paramRegistry.RegisterScript(new AutoRefresh())
    paramRegistry.RegisterScript(new AddJquery())
    paramRegistry.RegisterScript(new AddLodash())
    paramRegistry.RegisterScript(new AddUnderscore())
    paramRegistry.RegisterScript(new WikiCopy())
    paramRegistry.RegisterScript(new OshTest())
} catch (err) {
    Err('Error registering params.')
    Err(err)
}

function Help () {
    let names = paramRegistry.Scripts.map(s => {
        return { text: s.Help || s.Name }
    })

    let scriptHelp = scriptRegistry.Scripts.filter(s => s.Help).map(s => {
        return { text: s.Help }
    })

    Box({
        title: 'HELP',
        lines: [
            { text: 'oshstop - Stops Osh from running' },
            { text: 'oshdebug - Trigger debuggers' },
            { text: 'oshupdate - Trigger update' },
            { text: 'osh.help - What you just did' },
            { text: 'PARAMS', subhead: true },
            ...names,
            { text: 'SCRIPTS', subhead: true },
            ...scriptHelp
        ]
    })
}

function UrlUpdated () {
    try {
        if (HasParam('osh.help')) Help()
        else Log('Use "osh.help" param for more info.')

        scriptRegistry.RunScripts()
        paramRegistry.RunScripts()
    } catch (err) {
        Err('Error running scripts.')
        Err(err)
    }
}

let previousUrl = null
function WatchUrl () {
    if (window.location.href !== previousUrl) {
        Log(`Url changed to ${window.location.href}`)
        UrlUpdated()
        previousUrl = window.location.href
    }

    setTimeout(() => WatchUrl(), WATCH_URL_INTERVAL)
}

$(() => WatchUrl())
