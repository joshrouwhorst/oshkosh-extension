let sendParentFunc

window.oshkosh = {
    receive
}

function receive(func) {
    sendParentFunc = func
    console.log('Received func')
    setTimeout(() => send(), 1000)
}

function send() {
    console.log('Sending')
    sendParentFunc('Testing 123')
}
