const $ = require('jquery')
const logger = require('./logger')
const GIT_BASE = 'https://gitlab.com/joshrouwhorst/oshkosh-extension/-/raw/master'
const GIT_SERVE_BASE = 'https://joshrouwhorst.gitlab.io/oshkosh-extension/public'

function GetQueryVariable(variable) {
    try {
        let query = null

        if (window.location.search) {
            query = window.location.search.substring(1)
        } else if (window.location.hash) {
            query = window.location.hash
            if (query.indexOf('?') === -1) return null
            query = query.split('?')[1]
        }

        if (!query) return null

        let vars = query.split('&')
        for (let i = 0; i < vars.length; i++) {
            const pair = vars[i].split('=')
            if (pair.length === 1) continue
            if (decodeURIComponent(pair[0]) === variable) {
                return decodeURIComponent(pair[1])
            }
        }
    } catch (err) {
        console.error(err)
    }

    return null
}

function GetAriaLabel(elem) {
    const $elem = $(elem)
    const label = $elem.attr('aria-label')
    if (label) return label

    const id = $elem.attr('aria-labelledby')
    if (id) return $(`#${id}`).text()
}

function HasParam(name) {
    return window.location.search.indexOf(name) > -1 || window.location.hash.indexOf(name) > -1;
}

module.exports = {
    GIT_BASE,
    GIT_SERVE_BASE,
    GetQueryVariable,
    GetAriaLabel,
    HasParam,
    $,
    ...logger
}