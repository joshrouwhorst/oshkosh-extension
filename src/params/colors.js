const { $ } = require('../utils')
const Param = require('../models/param')

class Color extends Param {
    constructor(data) {
        super(data)

        this.Filter = null
        this.Postfix = null
        this.Ranged = false

        const { filter, postfix, ranged } = data
        if (filter) this.Filter = filter
        if (postfix) this.Postfix = postfix

        if (ranged) {
            this.Ranged = true
            this.Postfix = '%'
        }
    }

    Run(amount) {
        if (this.Ranged) {
            amount = parseInt(amount, 10)
            amount += 100
            amount = amount.toString()
        }

        amount += this.Postfix

        let existing = $('body').css('filter') || ''
        if (existing.toLowerCase() === 'none') existing = ''
        if (existing) existing += ' '

        const val = `${existing}${this.Filter}(${amount})`
        $('body').css('filter', val)
    }
}

class Gray extends Color {
    constructor() {
        super({
            name: 'colors.gray',
            filter: 'grayscale',
            postfix: '%',
            defaultValue: '100',
            help: 'colors.gray - values: 0-100 - default value: 100'
        })
    }
}

class Invert extends Color {
    constructor() {
        super({
            name: 'colors.invert',
            filter: 'invert',
            postfix: '%',
            defaultValue: '100',
            help: 'colors.invert - values: 0-100 - default value: 100'
        })
    }
}

class Contrast extends Color {
    constructor() {
        super({
            name: 'colors.cont',
            filter: 'contrast',
            ranged: true,
            defaultValue: '100',
            help: 'colors.cont - values: 0-infinite - default value: 100'
        })
    }
}

class Brightness extends Color {
    constructor() {
        super({
            name: 'colors.bright',
            filter: 'brightness',
            ranged: true,
            defaultValue: '100',
            help: 'colors.bright - values: 0-infinite - default value: 100'
        })
    }
}

class Saturate extends Color {
    constructor() {
        super({
            name: 'colors.sat',
            filter: 'saturate',
            ranged: true,
            defaultValue: '100',
            help: 'colors.sat - values: 0-infinite - default value: 100'
        })
    }
}

class Hue extends Color {
    constructor() {
        super({
            name: 'colors.hue',
            filter: 'hue-rotate',
            postfix: 'deg',
            defaultValue: '90',
            help: 'colors.hue - values: 0-360 - default value: 90'
        })
    }
}

module.exports = {
    Gray,
    Invert,
    Contrast,
    Brightness,
    Saturate,
    Hue
}