const { Log, GIT_SERVE_BASE } = require('../utils')
const Param = require('../models/param')

class OshTest extends Param {
    constructor() {
        super({
            name: 'osh.test'
        })
    }

    Run() {
        Log(`Opening popup`)
        var childWindow = window.open(`${GIT_SERVE_BASE}/popup`)

        setTimeout(() => {
            if (childWindow.oshkosh) {
                Log('Oshkosh object found')
                childWindow.oshkosh.receive((text) => {
                    Log(`Text Received: ${text}`)
                })
            } else {
                Log('Oshkosh object NOT found')
            }
        }, 5000)
    }
}

module.exports = {
    OshTest
}