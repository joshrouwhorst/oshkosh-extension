const { $, Err } = require('../utils')
const Param = require('../models/param')

class TeamworkBullet extends Param {
    constructor() {
        super({ name: 'bullet.list' })
    }

    Run() {
        if ($('.board-column').length <= 0) {
            setTimeout(() => this.Run(), 1000)
        } else {
            setTimeout(() => this.CreateLists(), 5000)
        }
    }

    CreateLists() {
        let lists = ''

        $('.board-column').each((idx, col) => {
            try {
                const $col = $(col)
                const title = $col.find('.boards-column--header-title').text().trim()

                let names = ''
                $col.find('.task-card .name').each((idx, name) => {
                    const $name = $(name)
                    const text = $name.text().trim()
                    if (text) names += `<li>${text}</li>`
                })

                if (names) lists += `<div><h2>${title}</h2><ul>${names}</ul><div>`
            } catch (err) {
                Err(err)
            }

        })

        $('#mainContent').prepend(`<div id="st-lists">${lists}</div>`)
    }
}

class TeamworkTable extends Param {
    constructor() {
        super({ name: 'table.list' })
    }

    Run() {
        if ($('.board-column').length <= 0) {
            setTimeout(() => this.Run(), 1000)
        } else {
            setTimeout(() => this.CreateLists(), 5000)
        }
    }

    CreateLists() {
        let lists = ''

        $('.board-column').each((idx, col) => {
            try {
                const $col = $(col)
                const title = $col.find('.boards-column--header-title').text().trim()

                let names = ''
                $col.find('.task-card').each((idx, task) => {
                    const $task = $(task)
                    const $name = $task.find('.name')
                    const $estimate = $task.find('[aria-label="Estimated Time"]')
                    let estimate = $estimate.text().trim()
                    if (estimate) estimate = estimate.match(/\d+/g).join('')
                    const text = $name.text().trim()
                    if (text) names += `<tr><td>${text}</td><td>${estimate}</td></tr>`
                })

                if (names) lists += `<div><h2>${title}</h2><table>${names}</table><div>`
            } catch (err) {
                Err(err)
            }

        })

        $('#mainContent').prepend(`<div id="st-lists">${lists}</div>`)
    }
}

module.exports = {
    TeamworkBullet,
    TeamworkTable
}
