const { Log } = require('../utils')
const Param = require('../models/param')

class AutoRefresh extends Param {
    constructor() {
        super({
            name: 'auto.refresh',
            defaultValue: '30',
            help: 'auto.refresh - values: seconds - default value: 30'
        })
    }

    Run(value) {
        value = parseInt(value, 10)
        Log(`AutoRefresh will refresh the page in ${value} seconds...`)

        setTimeout(() => {
            window.location.reload()
        }, value * 1000)
    }
}

module.exports = {
    AutoRefresh
}