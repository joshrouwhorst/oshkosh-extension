const { $, Err } = require('../utils')
const Param = require('../models/param')

class WikiCopy extends Param {
    constructor() {
        super({ name: 'wiki.copy' })
    }

    Run() {
        $('sup.reference').replaceAll('<span>**^** </span>')
    }
}

module.exports = {
    WikiCopy
}
