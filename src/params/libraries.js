const { $ } = require('../utils')
const Param = require('../models/param')

class AddJquery extends Param {
    constructor() {
        super({ name: 'add.jquery' })
    }

    Run() {
        $('body').append(`<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>`)
    }
}

class AddLodash extends Param {
    constructor() {
        super({ name: 'add.lodash' })
    }

    Run() {
        $('body').append(`<script
        src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"
        crossorigin="anonymous"></script>`)
    }
}

class AddUnderscore extends Param {
    constructor() {
        super({ name: 'add.underscore' })
    }

    Run() {
        $('body').append(`<script
        src="https://cdn.jsdelivr.net/npm/underscore@1.13.1/underscore-umd-min.js"
        crossorigin="anonymous"></script>`)
    }
}

module.exports = {
    AddJquery,
    AddLodash,
    AddUnderscore
}