const { $ } = require('../utils')
const Param = require('../models/param')

class FlipX extends Param {
    constructor() {
        super({ name: 'flip.x' })
    }

    Run() {
        let existing = $('body').css('transform') || ''
        if (existing.toLowerCase() === 'none') existing = ''
        if (existing) existing += ' '

        const val = `${existing}scaleX(-1)`
        $('body').css('transform', val)
    }
}

class FlipY extends Param {
    constructor() {
        super({ name: 'flip.y' })
    }

    Run() {
        let existing = $('body').css('transform') || ''
        if (existing.toLowerCase() === 'none') existing = ''
        if (existing) existing += ' '

        const val = `${existing}scaleY(-1)`
        $('body').css('transform', val)
    }
}

module.exports = {
    FlipX,
    FlipY
}
