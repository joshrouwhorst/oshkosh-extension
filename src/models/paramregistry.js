const { Log, Err, GetQueryVariable, HasParam } = require('../utils')

class ParamRegistry {
    constructor() {
        this.Scripts = []
        this.ActiveScripts = []
    }

    RunScripts() {
        if (this.ActiveScripts.length === 0) return

        const names = this.ActiveScripts.map(s => `${s.param.Name}=${s.value}`)
        Log(`Params ${names.join(', ')}`)
        this.ActiveScripts.forEach(s => {
            try {
                s.param.Run(s.value)
            } catch (err) {
                Err(`Error running param ${s.param.Name}.`)
                Err(err)
            }
        })
    }

    RegisterScript(param) {
        this.Scripts.push(param)

        let match = false
        if (!param.Name || HasParam(param.Name)) match = true

        if (match) {
            let value = GetQueryVariable(param.Name)
            if (!value) value = param.Default
            this.ActiveScripts.push({ param, value })
        }
    }
}

module.exports = {
    ParamRegistry
}