class Param {
    constructor({ name, defaultValue, help }) {
        this.Name = null
        this.Default = null
        this.Help = null
        if (name) this.Name = name
        if (defaultValue) this.Default = defaultValue
        if (help) this.Help = help
    }
}

module.exports = Param