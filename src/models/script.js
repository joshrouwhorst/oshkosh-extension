class Script {
    constructor({ name, rules, help }) {
        this.Name = null
        this.Rules = null
        this.Help = null
        if (name) this.Name = name
        if (rules) this.Rules = rules
        if (help) this.Help = help
    }
}

module.exports = Script
