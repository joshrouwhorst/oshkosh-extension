const { Log, GetQueryVariable } = require('../utils')

class ScriptRegistry {
    constructor() {
        this.Scripts = []
        this.ActiveScripts = []
    }

    RunScripts() {
        if (window.location.href.indexOf('stop.scripts') > -1) return

        const stopScript = GetQueryVariable('stop.script')
        if (!!stopScript) {
            let idx = this.ActiveScripts.findIndex((s) => s.Name === stopScript)
            if (idx > -1) this.ActiveScripts.splice(idx, 1)
        }

        if (this.ActiveScripts.length === 0) return

        const names = this.ActiveScripts.map(s => s.Name)
        Log(`Running ${names.join(', ')}`)
        this.ActiveScripts.forEach(s => s.Run())
    }

    RegisterScript(script) {
        this.Scripts.push(script)

        let match = false
        if (!script.Rules) match = true
        else {
            if (!match && script.Rules.includes) match = script.Rules.includes.some(i => window.location.href.indexOf(i) > -1)
            if (match && script.Rules.excludes) match = !script.Rules.excludes.some(i => window.location.href.indexOf(i) > -1)
        }

        if (match) this.ActiveScripts.push(script)
    }
}

module.exports = {
    ScriptRegistry
}