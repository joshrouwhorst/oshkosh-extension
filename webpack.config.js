const mode = 'production'
const popupConfig = require('./webpack.popup.js')

const defaultOpts = {
    optimization: {
        minimize: false
    },
    mode
}

const configs = [
    {
        entry: './src/oshkosh.js',
        output: {
            path: __dirname + '/extension',
            filename: 'oshkosh.js'
        }
    },
    {
        entry: './src/index.js',
        output: {
            path: __dirname + '/extension',
            filename: 'index.js'
        }
    },
    popupConfig
]

for (let i = 0; i < configs.length; i++) {
    configs[i] = {
        ...defaultOpts,
        ...configs[i]
    }
}

module.exports = configs